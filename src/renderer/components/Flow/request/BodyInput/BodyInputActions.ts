type SelectRequestMessageName = {
  type: 'SELECT_REQUEST_MESSAGE_NAME';
  name: string;
};

const SELECT_REQUEST_MESSAGE_NAME = 'SELECT_REQUEST_MESSAGE_NAME';

type SelectBodyType = {
  type: 'SELECT_BODY_TYPE';
  bodyType: string;
};

const SELECT_BODY_TYPE = 'SELECT_BODY_TYPE';

type SelectBase64Encode = {
  type: 'SELECT_BASE64_ENCODE';
  isBase64Encode: boolean;
};

const SELECT_BASE64_ENCODE = 'SELECT_BASE64_ENCODE';

type JSONBodyChangedType = {
  type: 'JSON_BODY_CHANGED_TYPE';
  bodyValue: string;
  isForceUpdate: boolean;
};

const JSON_BODY_CHANGED_TYPE = 'JSON_BODY_CHANGED_TYPE';

export const BodyInputActionTypes = [SELECT_REQUEST_MESSAGE_NAME, SELECT_BODY_TYPE, JSON_BODY_CHANGED_TYPE, SELECT_BASE64_ENCODE];
export type BodyInputActions = SelectRequestMessageName | SelectBodyType | JSONBodyChangedType | SelectBase64Encode;

export function selectRequestMessageName(name: string): SelectRequestMessageName {
  return {
    type: SELECT_REQUEST_MESSAGE_NAME,
    name,
  };
}

export function selectBodyType(bodyType: string): SelectBodyType {
  return {
    type: SELECT_BODY_TYPE,
    bodyType,
  };
}

export function JSONbodyChangedType(bodyValue: string, isForceUpdate: boolean): JSONBodyChangedType {
  return {
    type: JSON_BODY_CHANGED_TYPE,
    bodyValue,
    isForceUpdate
  };
}

export function selectBase64Encode(isBase64Encode: boolean): SelectBase64Encode {
  return {
    type: SELECT_BASE64_ENCODE,
    isBase64Encode,
  }
}