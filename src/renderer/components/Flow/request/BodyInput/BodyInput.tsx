import React, { FunctionComponent, ChangeEvent } from 'react';
import { Checkbox, Radio, Select } from 'antd';
import { RadioChangeEvent } from 'antd/lib/radio';
import TextArea from 'antd/lib/input/TextArea';
import MessageValueView, { dispatchingHandler } from '../../body/MessageValueView';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';
import { selectRequestMessageName, selectBodyType, JSONbodyChangedType, selectBase64Encode } from './BodyInputActions';
import { BodyType, RequestBody } from '../../../../models/request_builder';
import { ProtoCtx } from '../../../../../core/protobuf/protobuf';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';

type Props = {
  bodyType: BodyType;
  bodies: RequestBody;
  protoCtx: ProtoCtx;
  messageNames: ReadonlyArray<string>;
  isBase64Enable: boolean,
};

const BodyWrapper = styled('div')`
  display: block;
  margin-top: 8px;
`;

export const MESSAGE_NAME_WIDTH = 500;

export const JSON_BODY_AREA_WIDTH = 500;
export const JSON_BODY_AREA_HEIGHT = 100;


const BodyInput: FunctionComponent<Props> = ({ bodyType, bodies, protoCtx, messageNames, isBase64Enable }) => {
  const dispatch = useDispatch();
  //const [JSONVal, setJSONVal] = React.useState<any>(undefined);
 // const [requestMsg, setRequestMsg] = React.useState<any>(bodies.protobuf?.type.name);
  const [isRequestChange, setIsRequestChange] = React.useState<boolean>(false);
  function onRadioChange(e: RadioChangeEvent): void {
    dispatch(selectBodyType(e.target.value));
    //dispatch(JSONbodyChangedType(JSONVal, true));
  }

  function onJSONBodyChange(e: ChangeEvent<HTMLTextAreaElement>): void {
    //setJSONVal(e.target.value);
    dispatch(JSONbodyChangedType(e.target.value, true));
  }

  function onSelectRequestMsg(msgName: string): void {
    setIsRequestChange(true);
    dispatch(selectRequestMessageName(msgName));
  }

  function onCheckboxChange(e: CheckboxChangeEvent): void {
    dispatch(selectBase64Encode(e.target.checked));
  }

  React.useEffect(()=>{
    if(bodies.protobuf && isRequestChange) {
      const bodiesJSON = getJSON(bodies.protobuf);
      //setRequestMsg(bodies.protobuf.type.name);
     // setJSONVal(JSON.stringify(bodiesJSON, null, "\t"));
      setIsRequestChange(false);
      dispatch(JSONbodyChangedType(JSON.stringify(bodiesJSON, null, "\t"), true));
    }
  },[bodies.protobuf])

  function getJSON( data: any) {
    const { singleFields, mapFields, repeatedFields, oneOfFields } = data!;
    let res : any = {};
    singleFields.forEach(([fieldName, value]: any)=> {
      if(value.type.tag == "message") {
        res[fieldName] = getJSON( value); 
      } else {
        res[fieldName] = "" ; 
      }
    })
    mapFields!.forEach((fieldName: any) => {
      res[fieldName[0]] = {} ; 
    });
    repeatedFields!.forEach((fieldName: any) => {
      res[fieldName[0]] = [] ; 
    })
    oneOfFields!.forEach(([fieldName, value]: any) => {
      const a = value[0]; 
        res[fieldName] = value.length > 0 ? {
          [a] : getJSON(value[1])
        } : {}; 
    });
    return res;
  }

  const handlers = dispatchingHandler(dispatch, protoCtx);

  function renderBody(): React.ReactNode {
    return bodyType === 'none' ? (
      <div />
    ) : bodyType === 'protobuf' ? (
      <>
        <div style={{ marginBottom: 8 }}>
          <span>Request Message: </span>
          <Select
            value={bodies.protobuf && bodies.protobuf.type.name}
            onChange={onSelectRequestMsg}
            size="small"
            style={{ width: MESSAGE_NAME_WIDTH }}
            showSearch
            filterOption={(input, option): boolean => {
              return option && option.value.toString().includes(input.toString());
            }}
          >
            {messageNames.map((messageName, idx) => (
              <Select.Option key={idx} value={messageName}>
                {messageName}
              </Select.Option>
            ))}
          </Select>
        </div>
        {bodies.protobuf ? <MessageValueView value={bodies.protobuf} handlers={handlers} editable /> : null}
      </>
    ) : bodyType === 'json' ? (
      <>
        <div style={{ marginBottom: 8 }}>
          <span>Request Body: </span>
          <br />
          <TextArea
            value={bodies.json}
            autoSize
            style={{ width: JSON_BODY_AREA_WIDTH, height: JSON_BODY_AREA_HEIGHT, resize: 'none' }}
            onChange={e => onJSONBodyChange(e)}
          ></TextArea>
        </div>
      </>
    ) : null;
  }

  return (
    <div>
      <Radio.Group defaultValue="none" value={bodyType} onChange={onRadioChange}>
        <Radio value="none">None</Radio>
        <Radio value="json">JSON</Radio>
        <Radio value="protobuf">Protobuf</Radio>
      </Radio.Group>
      <Checkbox onChange={onCheckboxChange} checked={isBase64Enable}>Enable Base64 encoding</Checkbox>
      <BodyWrapper>{renderBody()}</BodyWrapper>
    </div>
  );
};

export default BodyInput;
